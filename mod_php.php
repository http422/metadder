<?php
	/***************************************************************************************
	 *
	 *Function that adds a <meta....> tag specified in a  text file 
	 *  into every .html (can be other extensions) file in the same directory.
	 * Please perform a backup before running this script.
	 * 
	 * Written by Andrea Saw.
	 *
	 ***************************************************************************************/
	
	// CHANGE FILE EXTENSION AND DIRECTORY HERE.
	// DIRECTORY IS NOT RECURSIVELY ACCESSED TO PREVENT ACCIDENTAL ALTERATION,
	// PLEASE ENTER A SEPARATE DIRECTORY EACH TIME YOU WANT TO MODIFY FILES IN IT.
	$filelist = getDirectoryTree(".","html");
	
	// PATH OF THE FILE CONTAINING THE <META...> TAGS TO INSERT
	$file_name = "to_insert.txt";
	
	//we need to addslashes here because the processing stops at the first double apostrophe
	// escaping it immediately \" is the only way for us to process the contents \"
	$myFile = addslashes(file_get_contents($file_name));
	
	//create array of tags
	// tagArray[0] = "<meta http-equiv="Content-Language" content="en, de, jp, ko, cn">"
	// tagArray[1] = "<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">"		
	// :
	// and so forth
	//
	// also trimming the empty array elements
	$tagArray = array_filter(array_map('trim', explode("\n", $myFile)), 'strlen');
	
	foreach($tagArray as $string){
		
		//find the meta attribute and its corresponding value, and store them into $tagPieces array
		// i also try to match \" and \' for the cases where the user enters <meta name='' content=""> 
		preg_match_all('/<meta (.+)=[\\\"|\\\'](.+)[\\\"|\\\'] (.+)=[\\\"|\\\'](.+)[\\\"|\\\']>/i', $string, $tagPieces,PREG_PATTERN_ORDER);
		//now we get rid of the backslash and apostrophes that were in the string \" or \'
		foreach($tagPieces as $a=>$b){
			foreach($b as $c=>$d){
				$tagPieces[$a][0] = preg_replace('/[\\\"\']+/', "", $d);
			}
		}
		
		
		//now we store the meta tags into the files we opened previously
		foreach ($filelist as $afile){
			
			$myDoc = new DOMDocument();	
			
			$myDoc->preserveWhiteSpace = false;
			
			$myDoc->loadHTMLFile($afile);				
			
			$myHead = $myDoc->getElementsByTagName('head')->item(0); 
			
			$myTag = $myDoc->createElement('meta');
			
			$newMetaTag = $myHead->appendChild($myTag);
			
			$newMetaTag->setAttribute($tagPieces[1][0], $tagPieces[2][0]);
			
			$newMetaTag->setAttribute($tagPieces[3][0], $tagPieces[4][0]);
			
			$myDoc->saveHTMLFile($afile);
		}
		
	}

	
	function getDirectoryTree( $outerDir , $x){
		
		// this removes the . and .. directories straight away
		$dirs = array_diff( scandir( $outerDir ), Array( ".", ".." ) ); 
		
		// declaring the results array to be returned later on
		$results_array = Array(); 
		
		foreach( $dirs as $d ){
			
			//getting the pathinfo (as its name implies 
			// it could be a path string but here it's a file name or directory)
			$path_parts = pathinfo($d);	
						
			//checking to see if the extensions match			
			if ($path_parts["extension"]==$x)	
				$results_array[] = $d;
		}
		
		//returns all matching file names
		return $results_array;
	
	}
	
	
	// Debugging area
	
	
	// Uncomment following line to check and see if allow_url_fopen is on.
	// This is needed to read from the file containing the tags 
	//   that we insert into all the files in a directory.
	//phpinfo();
	
?>